import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { IntroComponent } from './pages/home/intro/intro.component';
import { IntroTextComponent } from './pages/home/intro/intro-text/intro-text.component';
import { IntroImageComponent } from './pages/home/intro/intro-image/intro-image.component';
import { ListComponent } from './pages/home/list/list.component';
import { ListTitleComponent } from './pages/home/list/list-title/list-title.component';
import { ListCoverComponent } from './pages/home/list/list-cover/list-cover.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    IntroComponent,
    IntroTextComponent,
    IntroImageComponent,
    ListComponent,
    ListTitleComponent,
    ListCoverComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
