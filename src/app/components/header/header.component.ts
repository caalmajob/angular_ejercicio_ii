import { Component, OnInit } from '@angular/core';
import { Header } from './models/header';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public header: Header;
  public flag: boolean = false;
  constructor() {
    this.header = {
      icon: {
        img: '/assets/logo.png',
        name: 'Cosmere World',
      },
      links: ['HOME', 'ULTIMOS LANZAMIENTOS'],
    }
  }
  ngOnInit(): void {
  }

}
