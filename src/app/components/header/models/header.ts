export interface Header {
    icon: Icon;
    links: Array<string>;
    img?: string;
}

export interface Icon {
    img: string;
    name: string;
}