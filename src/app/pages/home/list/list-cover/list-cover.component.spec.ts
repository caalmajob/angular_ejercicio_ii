import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCoverComponent } from './list-cover.component';

describe('ListCoverComponent', () => {
  let component: ListCoverComponent;
  let fixture: ComponentFixture<ListCoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListCoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
