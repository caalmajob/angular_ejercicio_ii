import { Image } from './../../intro/models/intro';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-list-cover',
  templateUrl: './list-cover.component.html',
  styleUrls: ['./list-cover.component.scss']
})
export class ListCoverComponent implements OnInit {

  @Input() cover!: Image;

  @Output() emitMessage = new EventEmitter<string>();
  message: string = '';
  @Output() changeFont = new EventEmitter<boolean>();
  font: boolean = true;
  @Output() emitTitle= new EventEmitter<string>();
 
  @Input() boton: boolean;

  constructor() { }

  ngOnInit(): void {
  }
  sendMessage() {
    this.emitMessage.emit(this.message);
  }
  changeFonts() {
    this.changeFont.emit(this.font);
  }
  sendCover(message: string){
    this.emitMessage.emit(message);
  }
}
