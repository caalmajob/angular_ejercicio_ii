export interface List {
    listBook: Array<Books>;
    title: string;
    eventOne: String;
    eventTwo: string;
}

export interface Books {
    title: string;
    cover: Image;
}

export interface Image {
    image: string;
    name: string;

}