import { List } from './models/list';
import { Component, OnInit } from '@angular/core';
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  public coverMessage: any;
  public sonMessage: any;
  public sonFont: boolean;
  public boton: boolean = true;
  public list: List;
  constructor() {
    this.list = {
      listBook: [{
        title: 'Esquirla del Amanecer ',
        cover: {
          image: 'https://m.media-amazon.com/images/I/51FpVKB0okL.jpg',
          name: 'Esquirla del Amanecer '
        }
      },
      {
        title: 'El ritmo de la guerra',
        cover: {
          image: 'https://1.bp.blogspot.com/-ElrOkgRSG9I/XvnzuVb7_8I/AAAAAAAAZPY/Ox44n7j3KHgqniNqVyfGQySaI7T-F979ACNcBGAsYHQ/s1600/EbrMX4-XsAMyGlm.jpg',
          name: 'El ritmo de la guerra'
        }
      },
      {
        title: 'El Imperio Final',
        cover: {
          image: 'https://images-na.ssl-images-amazon.com/images/I/51F-WkRZu7L._SY264_BO1,204,203,200_QL40_ML2_.jpghttps://images-na.ssl-images-amazon.com/images/I/51F-WkRZu7L._SY264_BO1,204,203,200_QL40_ML2_.jpg',
          name: 'El Imperio Final'
        }
      },
      {
        title: 'Elantris',
        cover: {
          image: 'https://images-na.ssl-images-amazon.com/images/I/51n7pduOfML._SY264_BO1,204,203,200_QL40_ML2_.jpg',
          name: 'Elantris'
        }
      },
      {
        title: 'Steelheart',
        cover: {
          image: 'https://images-na.ssl-images-amazon.com/images/I/51OVaoubxnL._SY264_BO1,204,203,200_QL40_ML2_.jpg',
          name: 'Steelheart'
        }
      },
      {
        title: 'El camino de los reyes',
        cover: {
          image: 'https://images-na.ssl-images-amazon.com/images/I/518ggd0zZCL._SY264_BO1,204,203,200_QL40_ML2_.jpg',
          name: 'El camino de los reyes'
        }
      }
    ],
    title: 'Averigua el título:',
    eventOne: 'El libro es:',
    eventTwo: 'Show:'
    };
   }

  ngOnInit(): void {
  }
  setMessage(message: string): void {
    this.sonMessage = message;
  }
  setFont(font: boolean) {
  this.sonFont = font;
      }
setCover(coverTitle: string) {
  this.coverMessage = coverTitle;
 }
 
  changeOpacity(): void {
  this.boton = !this.boton;
 }
}
