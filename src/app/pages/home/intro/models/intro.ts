export interface Intro {
textIntro: string;
imageIntro: Image;
}

export interface Image {
    image: string;
    name: string;

}
