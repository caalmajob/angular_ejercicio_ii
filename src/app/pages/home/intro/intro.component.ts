import { Intro, Image } from './models/intro';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {
public intro: Intro;
public boton: boolean = false;
  constructor() {
    this.intro = {
      textIntro: 'Cosmere es la palabra que define el universo en el cual muchos de los libros de Brandon Sanderson tienen lugar. Es decir, a pesar de que las historias se desarrollan en diferentes mundos, cada uno de estos se encuentra en la misma galaxia, o cúmulo estelar. La estructura del universo literario del Cosmere es la misma que la nuestra. Fuerzas como la gravedad o el magnetismo, son idénticas a las que conocemos. Así mismo, el tiempo fluye al mismo ritmo que el nuestro. Por lo tanto, todas las novelas del Cosmere comparten una única cosmología, siguen las mismas reglas en cuanto a magia se refiere, y están conectadas por una historia general (aunque no todas las culturas que pueblan las páginas lo conocen). ',
      imageIntro: {
        image: 'https://3.bp.blogspot.com/-Fqdf_99d5no/WuLYgc0KViI/AAAAAAAAUQ0/BvMuZhnLhdciag9_lsXcgdpIvCeQ1Dr8wCLcBGAs/s1600/cosmere.jpg',
        name: 'cosmere'
      },
    }
  }

  ngOnInit(): void {
  }
  bigImage(): void {
    this.boton = !this.boton;
   }
}
