import { Image } from './../models/intro';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-intro-image',
  templateUrl: './intro-image.component.html',
  styleUrls: ['./intro-image.component.scss']
})
export class IntroImageComponent implements OnInit {
  @Input() image!: Image;
@Input() boton: boolean;
  constructor() { }

  ngOnInit(): void {
  }
}
