import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-intro-text',
  templateUrl: './intro-text.component.html',
  styleUrls: ['./intro-text.component.scss']
})
export class IntroTextComponent implements OnInit {


  public state: boolean = false;
  public info: string = 'A pesar de esto, Brandon ha dicho en reiteradas ocasiones que no se necesita tener ningún conocimiento del Cosmere para disfrutar las historias narradas en los diferentes mundos, cada una de ellas puede ser leída por separado, y sin necesidad de seguir un orden establecido. Pero, eventualmente será necesario tener una cierta comprensión de este universo literario para entender su trama. Toda la información que da lugar al Cosmere se está añadiendo lentamente a los libros en forma de pistas y comentarios, una especie de historia detrás de la historia que saltará a primer plano en futuras novelas.';
@Input() text!: string;
  constructor() { }

  ngOnInit(): void {
  }
  onButtonClick() : void {
    this.state  = !this.state;
   }

}
